��            )   �      �     �     �     �     �  
   �  7   �               0     <     C     H  "   [     ~     �     �     �     �  .   �            	   $  =   .     l  5   �     �  !   �     �  M       f  $   �     �     �  
   �  5   �     �            	   0     :     C  1   ]     �  &   �  %   �     �     �  /   �     -	  '   J	     r	  C   	     �	  :   �	     
  %   :
  $   `
                                               	                                                                                    
       Add configuration An unexpected error occured App ID App password App scopes Are you sure you want to delete the configuration '%s'? Cancel Configuration name Credentials Delete Edit Edit configuration Error requesting access token : %s List List of saved configurations Microsoft Graph authentication Name New configuration Provides methods to access Microsoft Graph API Save configuration Set default Tenant ID The selected configuration "%s" does not contain informations This user does not exist You must specify a unique name for this configuration You must specify the app ID You must specify the app password You must specify the tenant ID Project-Id-Version: LibMicrosoftGraph
PO-Revision-Date: 2020-07-23 15:18+0200
Last-Translator: Robin Bailleux <robin.bailleux@capwelton.com>
Language-Team: CapWelton<contact@capwelton.com>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Poedit-Basepath: ../programs
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: libmicrosoftgraph_translate;libmicrosoftgraph_translate:1,2;translate;translate:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: vendor
 Ajouter une configuration Une erreur inattendue s'est produite App ID Mot de passe App scopes Voulez-vous vraiment supprimer la configuration "%s"? Annuler Nom de la configuration Informations d'accès Supprimer Modifier Modifier la configuration Erreur lors de la requête du jeton d'accès : %s Liste Liste des configurations enregistrées Authentification avec Microsoft Graph Nom Nouvelle configuration Fourni une interface avec l'API Microsoft Graph Enregistrer la configuration Choisir comme configuration par défaut ID locataire La configuration sélectionnée "%s" ne contient pas d'informations Cet utilisateur n'existe pas Vous devez indiquer un nom unique pour cette configuration Vous devez spécifier l'App ID Vous devez spécifier le mot de passe Vous devez spécifier l'ID locataire 
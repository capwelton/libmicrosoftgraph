<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link https://www.capwelton.com})
 */

require_once dirname(__FILE__) . '/vendor/autoload.php';

function libmicrosoftgraph_translate($str, $str_plurals = null, $number = null)
{
	if ($translate = bab_functionality::get('Translate/Gettext'))
	{
		/* @var $translate Func_Translate_Gettext */
		$translate->setAddonName('LibMicrosoftGraph');
		
		return $translate->translate($str, $str_plurals, $number);
	}
	
	return $str;
}

/**
 * @return Func_MicrosoftGraph
 */
function libmicrosoftgraph_getFunctionnality()
{
    return bab_functionality::get('MicrosoftGraph');
}

function libmicrosoftgraph_redirect($url, $message = null)
{
    global $babBody;
    
    if (null === $url) {
        $babBody->addNextPageError($message);
        die('<script type="text/javascript">history.back();</script>');
    }
    
    if(isset($message)){
        $babBody->addNextPageError($message);
    }
    
    header('Location: ' . $url);
    die;
}
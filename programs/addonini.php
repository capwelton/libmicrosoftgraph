;<?php/*

[general]
name                            ="LibMicrosoftGraph"
version                         ="1.0.0"
description                     ="Microsoft Graph gateway functionnality"
description.fr                  ="Interface avec Microsoft Graph"
encoding                        ="UTF-8"
delete                          =1
ov_version                      ="7.0.0"
php_version                     ="5.1"
mysql_character_set_database    ="latin1,utf8"
addon_access_control            ="0"
author                          ="capwelton"
icon                            ="icon.png"
configuration_page              ="systemconf"

;*/?>

<?php

function libmicrosoftgraph_ovml($args)
{
    $ovContainer = array();
    
    if (!isset($args['action']))
    {
        trigger_error('Parameter \'action\' must be specified.');
        return $ovContainer;
    }
    
    switch ($args['action'])
    {
        case 'getLoginUrl':
            $ovContainer[] = array('loginUrl' => '?tg=login&sAuthType=MicrosoftGraph');
            break;
        default:
            break;
    }
    
    return $ovContainer;
}

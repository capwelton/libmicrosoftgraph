<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link https://www.capwelton.com})
 */

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';


/* @var $I Func_Icons */
$I = bab_functionality::get('Icons');
$I->includeCss();

/**
 *
 * @return Widget_Frame
 */
function libmicrosoftgraph_Editor()
{
    $W = bab_Widgets();
    $editor = $W->Frame();
    
    
    $layout = $W->VBoxItems(
        $W->LabelledWidget(
            libmicrosoftgraph_translate('Configuration name'),
            $W->LineEdit()
            ->setMandatory(true, libmicrosoftgraph_translate('You must specify a unique name for this configuration'))
            ->addClass('widget-fullwidth')
            ->setName('name')
        ),
        $W->Section(
            libmicrosoftgraph_translate('Credentials'),
            $W->VBoxItems(
                $W->LabelledWidget(
                    libmicrosoftgraph_translate('App ID'),
                    $W->LineEdit()
                    ->setMandatory(true, libmicrosoftgraph_translate('You must specify the app ID'))
                    ->addClass('widget-fullwidth')
                    ->setName('appId')
                ),
                $W->LabelledWidget(
                    libmicrosoftgraph_translate('Tenant ID'),
                    $W->LineEdit()
                    ->setMandatory(true, libmicrosoftgraph_translate('You must specify the tenant ID'))
                    ->addClass('widget-fullwidth')
                    ->setName('tenantId')
                ),
                $W->LabelledWidget(
                    libmicrosoftgraph_translate('App password'),
                    $W->LineEdit()
                    ->setMandatory(true, libmicrosoftgraph_translate('You must specify the app password'))
                    ->addClass('widget-fullwidth')
                    ->setName('appPwd')
                ),
                $W->LabelledWidget(
                    libmicrosoftgraph_translate('App scopes'),
                    $W->LineEdit()
                    ->setValue('openid profile offline_access user.read calendars.read')
                    ->addClass('widget-fullwidth')
                    ->setName('appScopes')
                )
            )
        )->addClass('box')
    )->setVerticalSpacing(1, 'em');
    
    $editor->setLayout($layout);
    return $editor;
}

/**
 *
 * @param string $configuration
 */
function libmicrosoftgraph_edit($name = null)
{
    $W = bab_Widgets();
    
    $addon = bab_getAddonInfosInstance('LibMicrosoftGraph');
    $addonUrl = $addon->getUrl();
    
    
    $page = $W->babPage();
    $page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
    
    
    
    $form = $W->Form();
    $form->addClass('BabLoginMenuBackground');
    $form->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
    $form->setName('data');
    
    $editor = libmicrosoftgraph_Editor();
    
    $form->addItem($editor);
    
    $form->addItem(
        $W->FlowItems(
            $W->SubmitButton()
            ->setLabel(libmicrosoftgraph_translate('Save configuration'))
            ->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK),
            $W->Link(
                libmicrosoftgraph_translate('Cancel'),
                $addonUrl . 'systemconf&idx=displayTpeList'
            )->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_CANCEL)
        )
        ->setSpacing(1, 'em')
        ->addClass(Func_Icons::ICON_LEFT_16)
    );
    
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', 'save');
    
    if (isset($name)) {
        $title = libmicrosoftgraph_translate('Edit configuration');
        
        $configuration = libmicrosoftgraph_getConfiguration($name);
        
        $name = array();
        foreach($configuration as $key => $value) {
            $name[$key] = $value;
        }
        
        $form->setValues(
            array(
                'data' => $name
            )
        );
        $form->setHiddenValue('data[originalName]', $name['name']);
    } else {
        $title = libmicrosoftgraph_translate('New configuration');
    }
    
    $page->setTitle($title);
    
    $page->addItem($form);
    
    $page->displayHtml();
}




function libmicrosoftgraph_save(Array $data)
{
    $configuration = new LibMicrosoftGraph_Configuration();
    
    foreach($configuration as $key => $dummy) {
        if (!property_exists($configuration, $key)) {
            continue;
        }
        $configuration->$key = $data[$key];
    }
    
    if (isset($data['originalName']) && $data['name'] != $data['originalName']) {
        libmicrosoftgraph_renameConfiguration($data['originalName'], $data['name']);
    }
    
    libmicrosoftgraph_saveConfiguration($data['name'], $configuration);
}




function libmicrosoftgraph_displayConfigurationList()
{
    $W = bab_Widgets();
    
    $addon = bab_getAddonInfosInstance('LibMicrosoftGraph');
    $addonUrl = $addon->getUrl();
    
    $page = $W->babPage();
    $page->setIconFormat(16, 'left');
    $page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
    
    $page->addItem($W->Title(libmicrosoftgraph_translate('List of saved configurations')));
    $tpeNames = libmicrosoftgraph_getConfigurationNames();
    
    $tableView = $W->TableView();
    
    $tableView->addItem(
        $W->Label(libmicrosoftgraph_translate('Name')),
        0, 0
    );
    $tableView->addItem(
        $W->Label(libmicrosoftgraph_translate('App ID')),
        0, 1
    );
    
    $tableView->addItem(
        $W->Label(''),
        0, 2
    );
    
    $tableView->addColumnClass(4, 'widget-column-thin');
    
    $tableView->addSection('configurations');
    $tableView->setCurrentSection('configurations');
    
    $defaultName = libmicrosoftgraph_getDefaultConfigurationName();
    
    $row = 0;
    foreach ($tpeNames as $name) {
        
        $configuration = libmicrosoftgraph_getConfiguration($name);
        
        $nameLabel = $W->Label($configuration->name);
        $isDefault = $name == $defaultName;
        $setDefaultLink = $W->Link(
            libmicrosoftgraph_translate('Set default'),
            $addonUrl . 'systemconf&idx=setDefaultConfiguration&name=' . $name
        );
        if ($isDefault) {
            $nameLabel->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK);
            $setDefaultLink = $W->Label(libmicrosoftgraph_translate('Set default'))->addClass('text-muted');
        }
        
        $tableView->addItem(
            $nameLabel->addClass(Func_Icons::ICON_LEFT_16),
            $row, 0
        );
        
        $tableView->addItem(
            $W->Label($configuration->appId),
            $row, 1
        );
        
        $tableView->addItem(
            $W->FlowItems(
                $W->Link(
                    libmicrosoftgraph_translate('Edit'),
                    $addonUrl . 'systemconf&idx=edit&name=' . $name
                )->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DOCUMENT_EDIT),
                $setDefaultLink->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DIALOG_OK),
                $W->Link(
                    libmicrosoftgraph_translate('Delete'),
                    $addonUrl . 'systemconf&idx=deleteConfiguration&name=' . $name
                )->setConfirmationMessage(sprintf(libmicrosoftgraph_translate('Are you sure you want to delete the configuration \'%s\'?'), $name))
                ->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_EDIT_DELETE)
            )
            ->setSpacing(4, 'px')
            ->addClass(Func_Icons::ICON_LEFT_16),
            $row, 2
        );
        
        $row++;
    }
    
    $page->addItem($tableView);
    
    $page->addItem(
        $W->FlowItems(
            $W->Link(
                libmicrosoftgraph_translate('Add configuration'),
                $addonUrl . 'systemconf&idx=edit'
            )->addClass('icon ' . Func_Icons::ACTIONS_LIST_ADD)
        )
    );
    
    
    $page->displayHtml();
}


/* main */

if (!bab_isUserAdministrator())
{
    return;
}



$idx= bab_rp('idx', 'displayConfigurationList');

$addon = bab_getAddonInfosInstance('LibMicrosoftGraph');

switch ($idx)
{        
    case 'edit':
        $name = bab_rp('name', null);
        libmicrosoftgraph_edit($name);
        break;
    case 'save':
        $data = bab_rp('data', null);
        libmicrosoftgraph_save($data);
        header('location:'. $addon->getUrl() .'systemconf&idx=displayConfigurationList');
        break;
    case 'deleteConfiguration':
        $name = bab_rp('name', null);
        libmicrosoftgraph_deleteConfiguration($name);
        header('location:'. $addon->getUrl() .'systemconf&idx=displayConfigurationList');
        break;
    case 'setDefaultConfiguration':
        $name = bab_rp('name', null);
        libmicrosoftgraph_setDefaultConfigurationName($name);
        header('location:'. $addon->getUrl() .'systemconf&idx=displayConfigurationList');
        break;
    case 'displayConfigurationList':
    default:
        $babBody->addItemMenu('list', libmicrosoftgraph_translate('List'), $GLOBALS['babAddonUrl'] . 'systemconf&idx=displayConfigurationList');
        libmicrosoftgraph_displayConfigurationList();
        break;
}

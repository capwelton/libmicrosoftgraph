<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link https://www.capwelton.com})
 */

require_once dirname(__FILE__) . '/functions.php';

class LibMicrosoftGraph_Configuration
{
    /**
     * The unique name of the configuration
     * @var string
     */
    public $name = '';
    
    /**
     * API account ID
     * @var string
     */
    public $appId = '';
    
    /**
     * 
     */
    public $tenantId = '';
    
    /**
     * API account password
     * @var string
     */
    public $appPwd = '';
    
    public $appScopes = 'openid profile offline_access user.read calendars.read';
    
    private $OAUTH_APP_ID;
    private $OAUTH_APP_PASSWORD;
    private $OAUTH_REDIRECT_URI;
    private $OAUTH_SCOPES;
    private $OAUTH_AUTHORITY;
    private $OAUTH_AUTHORIZE_ENDPOINT;
    private $OAUTH_TOKEN_ENDPOINT;
    
    public $env = array();
    
    private function initEnv()
    {
        global $babUrlScript;
        
        $this->OAUTH_APP_ID             = $this->appId;
        $this->OAUTH_APP_PASSWORD       = $this->appPwd;
        $this->OAUTH_REDIRECT_URI       = $babUrlScript.'?tg=login&cmd=signon&sAuthType=MicrosoftGraph';
        $this->OAUTH_SCOPES             = $this->appScopes;
        $this->OAUTH_AUTHORITY          = 'https://login.microsoftonline.com/'.$this->tenantId;
        $this->OAUTH_AUTHORIZE_ENDPOINT = '/oauth2/v2.0/authorize';
        $this->OAUTH_TOKEN_ENDPOINT     = '/oauth2/v2.0/token';
        
        $this->env = array(
            'OAUTH_APP_ID'              => $this->OAUTH_APP_ID,
            'OAUTH_APP_PASSWORD'        => $this->OAUTH_APP_PASSWORD,
            'OAUTH_REDIRECT_URI'        => $this->OAUTH_REDIRECT_URI,
            'OAUTH_SCOPES'              => $this->OAUTH_SCOPES,
            'OAUTH_AUTHORITY'           => $this->OAUTH_AUTHORITY,
            'OAUTH_AUTHORIZE_ENDPOINT'  => $this->OAUTH_AUTHORIZE_ENDPOINT,
            'OAUTH_TOKEN_ENDPOINT'      => $this->OAUTH_TOKEN_ENDPOINT
        );
    }
    
    public function getEnv()
    {
        if(empty($this->env)){
            $this->initEnv();
        }
        return $this->env;
    }
    
    public function getProvider()
    {
        $env = $this->getEnv();
        // Initialize the OAuth client
        return new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $env['OAUTH_APP_ID'],
            'clientSecret'            => $env['OAUTH_APP_PASSWORD'],
            'redirectUri'             => $env['OAUTH_REDIRECT_URI'],
            'urlAuthorize'            => $env['OAUTH_AUTHORITY'].$env['OAUTH_AUTHORIZE_ENDPOINT'],
            'urlAccessToken'          => $env['OAUTH_AUTHORITY'].$env['OAUTH_TOKEN_ENDPOINT'],
            'urlResourceOwnerDetails' => '',
            'scopes'                  => $env['OAUTH_SCOPES']
        ]);
    }
}

/**
 *
 * @return bab_registry
 */
function libmicrosoftgraph_getRegistry()
{
    $registry = bab_getRegistryInstance();
    
    $registry->changeDirectory('/LibMicrosoftGraph');
    
    return $registry;
}

/**
 * Returns the names of all configurations.
 *
 * @return array
 */
function libmicrosoftgraph_getConfigurationNames()
{
    $registry = libmicrosoftgraph_getRegistry();
    
    $registry->changeDirectory('configurations');
    
    $names = array();
    
    while ($name = $registry->fetchChildDir()) {
        $name = substr($name, 0, -1);
        $names[$name] = $name;
    }
    
    return $names;
}

/**
 * Returns all configuration information in a LibMicrosoftGraph_Configuration object.
 *
 * @param string $name		The configuration name.
 *
 * @return LibMicrosoftGraph_Configuration
 */
function libmicrosoftgraph_getConfiguration($name = null)
{
    $registry = libmicrosoftgraph_getRegistry();
    
    if (!isset($name)) {
        $name = libmicrosoftgraph_getDefaultConfigurationName();
    }
    
    $registry->changeDirectory('configurations');
    $registry->changeDirectory($name);
    
    $configuration = new LibMicrosoftGraph_Configuration();
    $properties = 0;
    
    while ($key = $registry->fetchChildKey()) {
        $configuration->$key = $registry->getValue($key);
        $properties++;
    }
    
    if (!$properties) {
        throw new Exception(sprintf(libmicrosoftgraph_translate('The selected configuration "%s" does not contain informations'), $name));
    }
    
    return $configuration;
}

/**
 * Sets the default configuration name.
 *
 * @param string $name		The configuration name.
 *
 * @return void
 */
function libmicrosoftgraph_setDefaultConfigurationName($name)
{
    $registry = libmicrosoftgraph_getRegistry();
    $registry->setKeyValue('default', $name);
}

/**
 * Returns the default configuration name.
 *
 * @return string
 */
function libmicrosoftgraph_getDefaultConfigurationName()
{
    $registry = libmicrosoftgraph_getRegistry();
    return $registry->getValue('default');
}

/**
 * Returns the default configuration.
 *
 * @return LibMicrosoftGraph_Configuration
 */
function libmicrosoftgraph_getDefaultConfiguration()
{
    return libmicrosoftgraph_getConfiguration(libmicrosoftgraph_getDefaultConfigurationName());
}

/**
 * Renames a configuration.
 *
 * @param string $originalName
 * @param string $newName
 *
 * @return bool
 */
function libmicrosoftgraph_renameConfiguration($originalName, $newName)
{
    $registry = libmicrosoftgraph_getRegistry();
    
    $registry->changeDirectory('configurations');
    
    return $registry->moveDirectory($originalName, $newName);
}

/**
 * Sets all configuration information.
 *
 * @param string name The name of the configuration to save.
 * @param LibMicrosoftGraph_Configuration $configuration
 *
 * @return void
 */
function libmicrosoftgraph_saveConfiguration($name, LibMicrosoftGraph_Configuration $configuration)
{
    $registry = libmicrosoftgraph_getRegistry();
    
    $registry->changeDirectory('configurations');
    $registry->changeDirectory($name);
    
    foreach($configuration as $key => $value) {
        
        $registry->setKeyValue($key, $value);
    }
    
}

/**
 * Deletes the specified configuration.
 
 * @param string name The name of the configuration to delete.
 * @return bool
 */
function libmicrosoftgraph_deleteConfiguration($name)
{
    $registry = libmicrosoftgraph_getRegistry();
    $registry->changeDirectory('configurations');
    
    if (!$registry->isDirectory($name)) {
        return false;
    }
    
    $registry->changeDirectory($name);
    
    $registry->deleteDirectory();
    
    return true;
}
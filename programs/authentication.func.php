<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link https://www.capwelton.com})
 */
require_once 'base.php';

use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

bab_functionality::includeFile('PortalAuthentication');

require_once $GLOBALS['babInstallPath'] . 'utilit/loginIncl.php';
require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';
require_once dirname(__FILE__) . '/TokenStore/TokenCache.php';

/**
 * @property $provider  \League\OAuth2\Client\Provider\GenericProvider
 */
class Func_PortalAuthentication_AuthMicrosoftGraph extends Func_PortalAuthentication
{
    private $provider = null;
    
	public function getDescription() 
	{
		return libmicrosoftgraph_translate("Microsoft Graph authentication");
	}
	
	private function getProvider()
	{
	    if(!isset($this->provider)){
	        $defaultConf = libmicrosoftgraph_getDefaultConfiguration();
	        $this->provider = $defaultConf->getProvider();
	    }
	    
	    return $this->provider;
	}
	
	private function authenticateWithGraph()
	{
	    $provider = $this->getProvider();
	    $authUrl = $provider->getAuthorizationUrl();
	    
	    // Save client state so we can validate in callback
	    $_SESSION['LibMicrosoftGraph_oauthState'] = $provider->getState();
	    
	    // Redirect to AAD signin page
	    libmicrosoftgraph_redirect($authUrl);
	}
	
	public function login()
	{
		if ($this->isLogged())
		{
			return true;
		}
		
		$authCode = bab_rp('code', null);
		
		if(!isset($authCode)){
		    $this->authenticateWithGraph();
		    return false;
		}
		
		$provider = $this->getProvider();
		
		try {
		    // Make the token request
		    $accessToken = $provider->getAccessToken('authorization_code', [
		        'code' => $authCode
		    ]);
		    
		    $graph = new Graph();
		    $graph->setAccessToken($accessToken->getToken());
		    
		    /* @var $user Model\User */
		    $user = $graph->createRequest('GET', '/me')
		    ->setReturnType(Model\User::class)
		    ->execute();
		    
		    $userId = bab_getUserIdByEmail($user->getMail());
		    if($userId == 0){
		        $this->addError(libmicrosoftgraph_translate('This user does not exist'));
		        return false;
		    }
		    
		    if ($this->userCanLogin($userId)) {
		        $this->setUserSession($userId, 315360000);
		        $tokenCache = new \LibMicrosoftGraph\TokenStore\TokenCache();
		        $tokenCache->storeTokens($accessToken, $user);
		        
		        $this->updateUserInfos($userId);
		    }
		    
		    return true;
		}
		catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
		    $this->addError(sprintf(libmicrosoftgraph_translate('Error requesting access token : %s'), $e->getMessage()));
		    return false;
		}
		
		$this->addError(libmicrosoftgraph_translate('An unexpected error occured'));
		return false;
	}
	
	
	public function logout()
	{
	    $AuthObject = bab_functionality::get('PortalAuthentication/AuthOvidentia');
	    return $AuthObject->logout();
	}
	
	private function updateUserInfos($userId)
	{
	    $func = libmicrosoftgraph_getFunctionnality();
	    $currentGraphUser = $func->getCurrentUser();
	    $userInfos  = bab_userInfos::getRow($userId);
	    
	    //Perform update with $currentGraphUser data;
	    
	    return true;
	}
}

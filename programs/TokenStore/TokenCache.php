<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link https://www.capwelton.com})
 */

namespace LibMicrosoftGraph\TokenStore;

require_once dirname(__FILE__) . '/../functions.php';
require_once dirname(__FILE__) . '/../configuration.php';

class TokenCache 
{
    public function storeTokens(\League\OAuth2\Client\Token\AccessToken $accessToken,\Microsoft\Graph\Model\User $user) {
        $this->setToken($accessToken);
        $this->setUser($user);
    }
    
    public function clearTokens() {
        unset($_SESSION['LibMicrosoftGraph_accessToken']);
        unset($_SESSION['LibMicrosoftGraph_refreshToken']);
        unset($_SESSION['LibMicrosoftGraph_tokenExpires']);
        unset($_SESSION['LibMicrosoftGraph_userName']);
        unset($_SESSION['LibMicrosoftGraph_userEmail']);
    }
    
    public function getAccessToken() {
        // Check if tokens exist
        if (empty($_SESSION['LibMicrosoftGraph_accessToken']) || empty($_SESSION['LibMicrosoftGraph_refreshToken']) || empty($_SESSION['LibMicrosoftGraph_tokenExpires'])){
            return '';
        }
            
        $token = $_SESSION['LibMicrosoftGraph_accessToken'];
        
        if($this->hasExpired()){
            global $babUrlScript;
            
            $defaultConf = libmicrosoftgraph_getDefaultConfiguration();
            $env = $defaultConf->getEnv();
            // Initialize the OAuth client
            $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
                'clientId'                => $env['OAUTH_APP_ID'],
                'clientSecret'            => $env['OAUTH_APP_PASSWORD'],
                'redirectUri'             => $env['OAUTH_REDIRECT_URI'],
                'urlAuthorize'            => $env['OAUTH_AUTHORITY'].$env['OAUTH_AUTHORIZE_ENDPOINT'],
                'urlAccessToken'          => $env['OAUTH_AUTHORITY'].$env['OAUTH_TOKEN_ENDPOINT'],
                'urlResourceOwnerDetails' => '',
                'scopes'                  => $env['OAUTH_SCOPES'],
                'return_url' 	          => $babUrlScript
            ]);
            
            $token = $oauthClient->getAccessToken('refresh_token', [
                'refresh_token' => $this->getRefreshToken()
            ]);
            
            $this->setToken($token);
        }
        return $token;
    }
    private function setUser(\Microsoft\Graph\Model\User $user)
    {
        $_SESSION['LibMicrosoftGraph_userName'] = $user->getDisplayName();
        $_SESSION['LibMicrosoftGraph_userEmail'] = null !== $user->getMail() ? $user->getMail() : $user->getUserPrincipalName();
        return $this;
    }
    
    private function setToken(\League\OAuth2\Client\Token\AccessToken $token)
    {
        $_SESSION['LibMicrosoftGraph_accessToken'] = $token->getToken();
        $_SESSION['LibMicrosoftGraph_refreshToken'] = $token->getRefreshToken();
        $_SESSION['LibMicrosoftGraph_tokenExpires'] = $token->getExpires();
        return $this;
    }
    
    public function getExpires()
    {
        return $_SESSION['LibMicrosoftGraph_tokenExpires'];
    }
    
    public function hasExpired()
    {
        $expires = $this->getExpires();
        
        if (empty($expires)) {
            return true;
        }
        
        return $expires < time();
    }
    
    public function getRefreshToken()
    {
        return $_SESSION['LibMicrosoftGraph_refreshToken'];
    }
}
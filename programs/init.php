<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link https://www.capwelton.com})
 */


function LibMicrosoftGraph_upgrade($version_base, $version_ini)
{
    $addon = bab_getAddonInfosInstance('LibMicrosoftGraph');    
    $addon->registerFunctionality('MicrosoftGraph', 'microsoftgraph.func.php');
    $addon->registerFunctionality('PortalAuthentication/AuthMicrosoftGraph', 'authentication.func.php');
    
    return true;
}


function LibMicrosoftGraph_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('LibMicrosoftGraph');
    $addon->unregisterFunctionality('MicrosoftGraph');
    $addon->unregisterFunctionality('PortalAuthentication/AuthMicrosoftGraph');
    return true;
}

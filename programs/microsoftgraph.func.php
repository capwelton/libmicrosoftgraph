<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link https://www.capwelton.com})
 */
require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';
require_once dirname(__FILE__) . '/TokenStore/TokenCache.php';

use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class Func_MicrosoftGraph extends bab_Functionality
{
    /**
     * @return string
     * @static
     */
    public function getDescription()
    {
        return libmicrosoftgraph_translate('Provides methods to access Microsoft Graph API');
    }
    
    /**
     * @return \Microsoft\Graph\Graph
     */
    public function getApi()
    {
        $tokenCache = new \LibMicrosoftGraph\TokenStore\TokenCache();
        $accessToken = $tokenCache->getAccessToken();
        
        // Create a Graph client
        $graph = new Graph();
        $graph->setAccessToken($accessToken);
        
        return $graph;
    }
    
    public function getCurrentUser()
    {
        $graph = $this->getApi();
        return $graph->createRequest('GET', '/me')
        ->setReturnType(Model\User::class)
        ->execute();
    }
}